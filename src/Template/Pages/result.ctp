<?php $this->layout = false; ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Result - Rush Hour - programmeertest DongIT</title>
		<?= $this->html->css('app') ?>
	</head>
	<body>
		<p>We have found the solution in <?= h($tries-1) ?> moves!</p>
		<?php #foreach(h($moves) as $move): ?>
			<!-- <p><?= $move ?></p>-->
		<?php #endforeach; ?>
		
		<?php $i = 1; foreach(h($grids) as $grid): ?>
				Grid <?= $i ?>:
			<?php for($row = 0, $rows = count($grid); $row < $rows; ++$row): ?>
				<div class="row">
					<?php for($col = 0, $cols = count($grid[$row]); $col < $cols; ++$col): ?>
						<span class='spot'><?= $grid[$row][$col] ?></span>
					<?php endfor; ?>
				</div>
			<?php endfor; ?>
		<?php ++$i; endforeach; ?>
	</body>
</html>