<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PagesController extends AppController
{
    private $cars = [],
            $tries = 0,
            $moves = [],
            $grids = [],
            $steps = [],
            $moves_queue = [],
            $grid_strings = [];


    /**
     * Render main game for user input
     */
    public function game()
    {
        $this->render('game');
    }


    /**
     * Attempt to find the solution based on the user input
     */
    public function findSolution()
    {
        $data = $this->request->data;

        switch ($data['_action']) {
            case 'update_settings': 
                return $this->redirect($data, '/');
            case 'calculate':
                $result = $this->calculateMoves($data['input_playfield']);

                $this->set('grids', $result['grids']);
                $this->set('moves', $result['moves']);
                $this->set('tries', $result['tries']);

                $this->render('result');
            
        }
    }

    /**
     * Calculate the moves to find the solution
     * 
     * @param Array $grid 
     * @return Array $result
     */
    private function calculateMoves($grid)
    {
        $this->addGrid($grid);

        // Find the cars in the grid, and the possible moves
        $this->findCars();
        $this->findMoves();


        // If the player car [R] can go to the exit, go to the exit,
        // otherwise try to go to the exit and try moves for other cars
        if ($this->canGoToExit()) {
            $this->goToExit();
        } else {
            $this->goToExit();
            $this->tryMoves();
        }

        return ['grids' => $this->grids, 'moves' => $this->moves, 'tries' => $this->tries];
    }


    /**
     * Add a grid to the grids array
     * 
     * @param Array $grid
     */
    private function addGrid($grid)
    {
        if (!$this->seen($grid)) $this->grids[] = $grid;
    }


    /**
     * Check if the player car [R] can go to the exit or not
     * 
     * @return Bool $returnType
     */
    private function canGoToExit()
    {
        $grid = $this->getLastGrid();

        $row = 3;

        $return = [];

        $cols = $this->getColumns($grid[$row]);

        $distance = $this->getDistanceToExit();

        $cols = [
            $grid[3][1],
            $grid[3][2],
            $grid[3][3],
            $grid[3][4],
            $grid[3][5],
            $grid[3][6],
            $grid[3][7]
        ];

        foreach ($cols as $col) {
            if ($col == '.' || $col == '@') $return[] = 1;
            else $return[] = 0;
        }

        $returnType = true;

        foreach ($return as $key => $val) {
            if ($val === 0) $returnType = false;
        }

        return $returnType;
    }

    /**
     * Get the distance to the exit from player car [R]
     * 
     * @return Integer $distance
     */
    private function getDistanceToExit()
    {
        $grid = $this->getLastGrid();

        $row = 3;

        $cols = $this->getColumns($grid[$row]);

        for ($col = 0; $col < $cols; ++$col) {
            $spot = $grid[$row][$col];

            if ($spot == 'R') {
                $current_pos = ['spot' => $spot, 'x' => $col, 'y' => 3];
            }
        }

        return (7 - $current_pos['x']);

    }

    /**
     * Move the player car [R] to the right if it can move right
     */
    private function goToExit()
    {
        do {
            $grid = $this->getLastGrid();
            $this->moveCarRight('R', $grid);
        } while ($this->canMoveRight('R', $grid));
    }

    /**
     * Find the cars in the grid
     */
    private function findCars()
    {
        $grid = $this->getLastGrid();

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                $spot = $grid[$row][$col];

                if (preg_match('/\w/', $spot)) {
                    if ($grid[$row-1][$col] == $spot || $grid[$row+1][$col] == $spot)
                        $this->cars[$spot] = 'vert';
                    else
                        $this->cars[$spot] = 'hori';
                }
            }
        }
    }

    /**
     * Find the possible moves in the grid
     */
    private function findMoves()
    {
        $cars = $this->cars;

        $grid = $this->getLastGrid();

        $moves = $this->moves;

        while ($sequence = array_shift($moves)) {
            foreach ($sequence as $step) {
                preg_match('/(\w)-(\w)(\d)/', $step, $data);
                $car = $data[1];
                $dir = $data[2];
                $rep = $data[3];

                for ($i = 0; $i < $rep; ++$i) {
                    if ($dir == 'R') $this->moveCarRight($car, $grid);
                    if ($dir == 'L') $this->moveCarLeft($car, $grid);
                    if ($dir == 'U') $this->moveCarUp($car, $grid);
                    if ($dir == 'D') $this->moveCarDown($car, $grid);
                }
            }
        }

        foreach ($cars as $car => $dir) {
            if ($dir == 'hori') {
                $left = $this->canMoveLeft($car, $grid);
                if ($left) $this->moves_queue[] = "{$car}-L{$left}";

                $right = $this->canMoveRight($car, $grid);
                if ($right) $this->moves_queue[] = "{$car}-R{$right}";
            } else {
                $up = $this->canMoveUp($car, $grid);
                if ($up) $this->moves_queue[] = "{$car}-U{$up}";

                $down = $this->canMoveDown($car, $grid);
                if ($down) $this->moves_queue[] = "{$car}-D{$down}";
            }
        }
    }


    /**
     * Try moves until the puzzle has been solved.
     */
    private function tryMoves()
    {
        $moves = $this->moves_queue;

        $grid = $this->getLastGrid();

        foreach ($moves as $step) {
            preg_match('/(\w)-(\w)(\d)/', $step, $data);
            $car = $data[1];
            $dir = $data[2];
            $rep = $data[3];

            for ($i = 0; $i < $rep; ++$i) {
                if ($dir == 'R') $this->moveCarRight($car, $grid);
                if ($dir == 'L') $this->moveCarLeft($car, $grid);
                if ($dir == 'U') $this->moveCarUp($car, $grid);
                if ($dir == 'D') $this->moveCarDown($car, $grid);
            }

            if ($this->isSolved($grid)) {
                return true;
            } else {
                if (!$this->seen($grid)) {
                    $this->steps[] = "{$car}-{$dir}{$rep}";
                    $this->moves_queue[] = $this->steps;
                    $this->tryMoves();
                }
            }
        }

        if (!$this->isSolved($this->getLastGrid())) {
            $this->goToExit();
            $this->tryMoves();
        }
    }


    /**
     * Check if a grid has been visited/seen before
     * 
     * @param Array $grid
     * @return Boolean $is_seen
     */
    private function seen($grid)
    {
        $s_grids = $this->grid_strings;
        $s_grid = $this->gridToString($grid);

        foreach ($s_grids as $ts_grid) {
            if ($ts_grid === $s_grid) return true;
        }

        $this->grid_strings[] = $s_grid;

        ++$this->tries;

        return false;
    }


    /**
     * Move car [$car] up by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Boolean $moved_up
     */
    private function moveCarUp($car, $grid)
    {
        if ($this->cars[$car] == 'hori') return false;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] == $car) {
                    if ($grid[$row-1][$col] != '.') return false;

                    $grid[$row-1][$col] = $car;
                    $grid[$row][$col] = '.';
                }
            }
        }

        $this->moves[] = $car.'-U1';

        $this->addGrid($grid);

        return true;
    }


    /**
     * Move car [$car] down by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Boolean $moved_down
     */
    private function moveCarDown($car, $grid)
    {
        if ($this->cars[$car] == 'hori') return false;

        $rows = $this->getRows($grid)-1;

        for ($row = $rows; $row >= 0; --$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] == $car) {
                    if ($grid[$row+1][$col] != '.') return false;

                    $grid[$row+1][$col] = $car;
                    $grid[$row][$col] = '.';
                }
            }
        }

        $this->moves[] = $car.'-D1';

        $this->addGrid($grid);

        return true;
    }

    /**
     * Move car [$car] left by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Boolean $moved_left
     */
    private function moveCarLeft($car, $grid)
    {
        if ($this->cars[$car] == 'vert') return false;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] != '.') return false;

                $grid[$row][$col-1] = $car;
                $grid[$row][$col] = '.';
            }
        }

        $this->moves[] = $car.'-L1';

        $this->addGrid($grid);

        return true;
    }

    /**
     * Move car [$car] right by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Boolean $moved_right
     */
    private function moveCarRight($car, $grid)
    {
        if ($this->cars[$car] == 'vert') return false;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row])-1;

            for ($col = $cols; $col > 0; --$col) {
                if ($grid[$row][$col] == $car) {
                    if ($grid[$row][$col+1] != '.') return false;

                    $grid[$row][$col+1] = $car;
                    $grid[$row][$col] = '.';
                }
            }
        }

        $this->moves[] = $car.'-R1';

        $this->addGrid($grid);

        return true;
    }


    /**
     * Check if car [$car] can move up by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Integer $move_count
     */
    private function canMoveUp($car, $grid)
    {
        if ($car == 'hori') return 0;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] == $car) {
                    $l = 0;

                    while ($grid[--$row][$col] == '.') ++$l;

                    return $l;
                }
            }
        }

        return 0;
    }

    /**
     * Check if car [$car] can move down by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Integer $move_count
     */
    private function canMoveDown($car, $grid)
    {
        if ($car == 'hori') return 0;

        $rows = $this->getRows($grid)-1;

        for ($row = $rows; $row >= 0; --$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] == $car) {
                    $l = 0;

                    while ($grid[++$row][$col] == '.') ++$l;

                    return $l;
                }
            }
        }

        return 0;
    }

    /**
     * Check if car [$car] can move left by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Integer $move_count
     */
    private function canMoveLeft($car, $grid)
    {
        if ($car == 'vert') return 0;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                if ($grid[$row][$col] == $car) {
                    $l = 0;

                    while ($grid[$row][--$col] == '.') ++$l;

                    return $l;
                }
            }
        }

        return 0;
    }


    /**
     * Check if car [$car] can move right by one spot
     * 
     * @param String $car
     * @param Array $grid
     * @return Integer $move_count
     */
    private function canMoveRight($car, $grid)
    {
        if ($car == 'vert') return 0;

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row])-1;

            for ($col = $cols; $col >= 0; --$col) {
                if ($grid[$row][$col] == $car) {
                    $l = 0;

                    while ($grid[$row][++$col] == '.') ++$l;

                    return $l;
                }
            }
        }

        return 0;
    }

    /**
     * Get the amount of rows in a grid
     * 
     * @param Array $grid
     * @return Integer $rows
     */
    private function getRows($grid)
    {
        return count($grid);
    }


    /**
     * Get the amount of columns in a grid row
     * 
     * @param Array $grid
     * @return Integer $columns
     */
    private function getColumns($row)
    {
        return count($row);
    }

    /**
     * Check if the grid has been solved
     * 
     * @param Array $grid
     * @return Integer $is_solved
     */
    private function isSolved($grid)
    {
        $s_grid = $this->gridToString($grid);

        if (preg_match('/R\@/', $s_grid)) return 1;

        return 0;
    }


    /**
     * Convert the grid to a string
     * 
     * @param Array $grid
     * @return String $s_grid
     */
    private function gridToString($grid)
    {
        $s_grid = '';

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                $s_grid .= $grid[$row][$col];
            }
        }

        return $s_grid;
    }


    /**
     * Convert the grid to a human readable format
     * 
     * @param Array $grid
     * @return String $s_grid
     */
    private function gridToHumanFormat($grid)
    {
        $s_grid = '';

        $rows = $this->getRows($grid);

        for ($row = 0; $row < $rows; ++$row) {
            $cols = $this->getColumns($grid[$row]);

            for ($col = 0; $col < $cols; ++$col) {
                $spot = $grid[$row][$col];

                $s_grid .= "<span class='spot'>{$spot}</span>";
            }

            $s_grid .= "<br />";
        }

        return $s_grid;
    }


    /**
     * Get the last grid that has been found
     * 
     * @return Array $last_grid
     */
    private function getLastGrid() {
        $last_grid = array_slice($this->grids, -1, 1);

        return $last_grid[0];
    }
}
?>