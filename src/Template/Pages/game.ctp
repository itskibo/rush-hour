<?php $this->layout = false; ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Rush Hour - programmeertest DongIT</title>
		<?= $this->html->css('app') ?>
	</head>
	<body>
		<form class="playfield-form" id="playfield-form" action="solution" method="POST">

			<div class="playfield">
				<!-- TOP WALL -->
				<div class="row">
					<?php for($a = 0, $b = 8; $a < $b; ++$a): ?>
						<input type="text" name="input_playfield[0][<?=$a?>]" maxlength="1" readonly class="input-playfield-wall" value="*" />
					<?php endfor; ?>
				</div>
				<!-- /TOP WALL -->

				<?php for($c = 0, $d = 6; $c < $d; ++$c): ?>
					<div class="row">
						<?php for($e = 0, $f = 8; $e < $f; ++$e): ?>

							<?php if ($e === 7 && $c === 2): ?>

								<input class="input-playfield-exit" id="input-playfield-exit" name="input_playfield[<?=$c+1?>][<?=$e?>]" value="@" readonly>

							<?php elseif($e === 0 || $e === 7): ?>

								<input class="input-playfield-wall" name="input_playfield[<?=$c+1?>][<?=$e?>]" type="text" maxlength="1" value="*" readonly />

							<?php else: ?>

								<?php if ($e === 1 && $c === 2 || $e === 2 && $c === 2): ?>
									<input class="input-playfield-car" id="input-playfield-car" name="input_playfield[<?=$c+1?>][<?=$e?>]" type="text" maxlength="1" readonly value="R" />
								<?php else: ?>
									<input class="input-playfield" name="input_playfield[<?=$c+1?>][<?=$e?>]" type="text" maxlength="1" />
								<?php endif;?>

							<?php endif; ?>

						<?php endfor; ?>
					</div>

				<?php endfor; ?>

				<!-- BOTTOM WALL -->
				<div class="row">

					<?php for($g = 0, $h = 8; $g < $h; ++$g): ?>
						<input class="input-playfield-wall" type="text" name="input_playfield[7][<?=$g?>]" maxlength="1" value="*" readonly />
					<?php endfor; ?>

				</div>
				<!-- /BOTTOM WALL -->
			</div>

			<div class="playfield-instructions">
				<span class="header">Playfield instructions</span>
				<span class="line">The player car is listed as R</span>
				<span class="line">The exit is listed as @</span>
				<span class="line">Cars should be listed A through J</span>
			</div>

			<hr class="divider" />

			<input type="hidden" name="_action" id="_action" value="" />
			<button class="input-submit" type="submit" name="action" id="submit-btn" value="calculate">Find solution</button>
		</form>

		<script type="text/javascript">
			(function() {
				var inputFields = document.getElementsByClassName("input-playfield"),
				    inputCar = document.getElementById("input-playfield-car"),
				    inputExit = document.getElementById("input-playfield-exit");

				for (var i = 0, j = inputFields.length; i < j; ++i) {
					inputFields[i].addEventListener("input", function() {
						this.value = this.value.toUpperCase();
					});
				}

				var playfieldForm = document.getElementById('playfield-form');

				var submitBtn = document.getElementById('submit-btn'),
				    actionField = document.getElementById('_action');

				submitBtn.addEventListener("click", function() {
					actionField.value = 'calculate';
				});

				playfieldForm.addEventListener("submit", function(e) {
					e.preventDefault();
					
					for (var i = 0, j = inputFields.length; i < j; ++i) {
						if (inputFields[i].value == null || inputFields[i].value == '') {
							inputFields[i].value = '.';
						}
					}

					this.submit();
				});
			})();
		</script>
	</body>
</html>